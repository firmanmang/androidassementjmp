package com.example.aplikasipendaftaransiswabaru.data;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "AplikasiPendaftaranSiswa.db";
    public static final String TABLE = "student";
    public static final String COLUMN_ID        = "id";
    public static final String COLUMN_NAME      = "name";
    public static final String COLUMN_ADDRESS   = "address";
    public static final String COLUMN_PHONE     = "phone";
    public static final String COLUMN_GENDER    = "gender";
    public static final String COLUMN_LOCATION  = "location";
    public static final String COLUMN_IMAGE     = "image";

    public DBHelper(@Nullable Context context) {
        super(context, DB_NAME, null, 1);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable =
                "CREATE TABLE " + TABLE +" ("
                        + COLUMN_ID +" INTEGER PRIMARY KEY,"
                        + COLUMN_NAME + " TEXT,"
                        + COLUMN_ADDRESS + " TEXT,"
                        + COLUMN_PHONE + " TEXT,"
                        + COLUMN_GENDER + " TEXT,"
                        + COLUMN_LOCATION + " TEXT,"
                        + COLUMN_IMAGE + " TEXT)";

        db.execSQL(createTable);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE );
        onCreate(db);
    }

    public void insert(Student student) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME, student.getName());
        contentValues.put(COLUMN_ADDRESS, student.getAddress());
        contentValues.put(COLUMN_PHONE, student.getPhone());
        contentValues.put(COLUMN_GENDER, student.getGender());
        contentValues.put(COLUMN_LOCATION, student.getLocation());
        contentValues.put(COLUMN_IMAGE, student.getImage());

        db.insert(TABLE, null,contentValues);
    }


    @SuppressLint("Range")
    private Student parseStudent(Cursor rawQuery) {
        int studentId        = rawQuery.getInt(rawQuery.getColumnIndex(COLUMN_ID));
        String studentName      = rawQuery.getString(rawQuery.getColumnIndex(COLUMN_NAME));
        String studentAddress   = rawQuery.getString(rawQuery.getColumnIndex(COLUMN_ADDRESS));
        String studentPhone     = rawQuery.getString(rawQuery.getColumnIndex(COLUMN_PHONE));
        String studentGender    = rawQuery.getString(rawQuery.getColumnIndex(COLUMN_GENDER));
        String studentLocation  = rawQuery.getString(rawQuery.getColumnIndex(COLUMN_LOCATION));
        String studentImage     = rawQuery.getString(rawQuery.getColumnIndex(COLUMN_IMAGE));

        Student student = new Student(studentName, studentAddress,
                studentPhone, studentGender,
                studentLocation, studentImage);
        student.setId(studentId);
        return student;
    }

    public Student get(int id) {
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("SELECT * FROM %s WHERE %s LIKE '%d'",
                TABLE, COLUMN_ID, id);
        Cursor rawQuery = db.rawQuery(query, null);
        rawQuery.moveToFirst();

        Student student = parseStudent(rawQuery);

        return student;
    }


    public List<Student> getAll() {
        List<Student> students = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("SELECT * FROM %s", TABLE);
        Cursor rawQuery = db.rawQuery(query, null);
        rawQuery.moveToFirst();

        while (! rawQuery.isAfterLast()) {
            students.add(parseStudent(rawQuery));
            rawQuery.moveToNext();
        }

        return students;
    }
}
