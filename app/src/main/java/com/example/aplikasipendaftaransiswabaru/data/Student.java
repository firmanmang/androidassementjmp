package com.example.aplikasipendaftaransiswabaru.data;

public class Student {

    private int Id;
    private String name;
    private String address;
    private String phone;
    private String gender;
    private String location;
    private String image;

    public Student(String name,
                   String address,
                   String phone,
                   String gender,
                   String location,
                   String image) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.gender = gender;
        this.location = location;
        this.image = image;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
