package com.example.aplikasipendaftaransiswabaru;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.aplikasipendaftaransiswabaru.data.DBHelper;
import com.example.aplikasipendaftaransiswabaru.data.Student;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class MainActivity extends AppCompatActivity {

    private ExtendedFloatingActionButton addFab;

    private ListView studentsList;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imageView = findViewById(R.id.image_preview);
        studentsList = findViewById(R.id.main_list);
        DBHelper db = new DBHelper(getBaseContext());

        Student[] students = db.getAll().toArray(new Student[0]);
        StudentArrayAdapter adapter = new StudentArrayAdapter(this, students);
        studentsList.setAdapter(adapter);



        studentsList.setOnItemClickListener((adapterView, view, i, l) -> {
            openDetail(students[i]);
        });

        addFab = findViewById(R.id.add_fab);

        addFab.setOnClickListener(view -> {
            Intent intent = new Intent(this, AddFormActivity.class);
            startActivity(intent);
        });
    }

    private void openDetail(Student student) {
        Intent intent = new Intent(this, DetailStudentActivity.class);
        intent.putExtra("student_id", student.getId());
        startActivity(intent);
    }


}