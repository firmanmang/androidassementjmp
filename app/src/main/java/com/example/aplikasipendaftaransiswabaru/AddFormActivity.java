package com.example.aplikasipendaftaransiswabaru;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.example.aplikasipendaftaransiswabaru.data.DBHelper;
import com.example.aplikasipendaftaransiswabaru.data.Student;
import com.google.android.material.textfield.TextInputEditText;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class AddFormActivity extends AppCompatActivity {
    private TextInputEditText nameInput;
    private TextInputEditText addressInput;
    private TextInputEditText phoneInput;
    private String gender = null;
    private Button saveBtn;
    private RadioButton pria;
    private RadioButton wanita;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private String latitude;
    private String longitude;
    private Button pickLocation;
    private Button pickImage;
    private ImageView imagePreview;
    private Bitmap bitmapImage;

    private final int SELECT_PICTURE = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_form);
        setTitle("Form Pendaftaran Siswa Baru");

        nameInput = findViewById(R.id.name_input);
        addressInput = findViewById(R.id.address_input);
        phoneInput = findViewById(R.id.no_hp_input);
        pria = findViewById(R.id.pria_selected);
        wanita = findViewById(R.id.wanita_selected);
        pickLocation = findViewById(R.id.pick_location);
        pickImage = findViewById(R.id.pick_image);
        imagePreview = findViewById(R.id.image_preview);
        saveBtn = findViewById(R.id.save_btn);

        DBHelper db = new DBHelper(getBaseContext());

        pria.setOnClickListener(view -> {
            pria.setChecked(true);
            wanita.setChecked(false);
        });

        wanita.setOnClickListener(view -> {
            wanita.setChecked(true);
            pria.setChecked(false);
        });

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(@NonNull Location location) {
                latitude = Double.toString(location.getLatitude());
                longitude = Double.toString(location.getLongitude());

                if(latitude != null && longitude != null) {
                    pickLocation.setText("Lokasi telah dipilih");
                    pickLocation.setBackgroundColor(Color.GRAY);
                    pickLocation.setClickable(false);
                }
            }

            @Override
            public void onProviderEnabled(@NonNull String provider) {
            }

            @Override
            public void onProviderDisabled(@NonNull String provider) {
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

        };

        pickLocation.setOnClickListener(view -> {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                onGPS();
            } else {
                getLocation();
            }
        });

        pickImage.setOnClickListener(view -> {
            imagePicker();
        });

        saveBtn.setOnClickListener(view -> {
            gender = (wanita.isChecked()) ? "wanita" : "pria";


            String name         = nameInput.getText().toString();
            String address      = addressInput.getText().toString();
            String phone        = phoneInput.getText().toString();
            String location     = latitude + ", " + longitude;
            String imageName    = "";

            if (bitmapImage != null) {
                imageName = saveToInternalStorage();
            }

            Student student = new Student(name, address, phone, gender, location, imageName);
            db.insert(student);

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        });

    }

    private void onGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Hidupkan GPS?")
                .setCancelable(false)
                .setPositiveButton("Yes", (dialog, which)
                        -> startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                )
                .setNegativeButton("No", (dialog, which)
                        -> dialog.cancel());

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getLocation() {
        if ((ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)
                && ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
//            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    30_000, 0,
                    locationListener
            );
        }
    }

    void imagePicker() {
        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(i, "Select Picture"), SELECT_PICTURE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                if (null != selectedImageUri) {
                    imagePreview.setImageURI(selectedImageUri);
                    bitmapImage = ((BitmapDrawable)imagePreview.getDrawable()).getBitmap();
                }
            }
        }
    }

    private String saveToInternalStorage(){

        String name = Helper.randomString()+".jpg";
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        File mypath = new File(directory,name);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return name;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (locationManager != null && locationListener != null) {
            locationManager.removeUpdates(locationListener);
        }
    }
}