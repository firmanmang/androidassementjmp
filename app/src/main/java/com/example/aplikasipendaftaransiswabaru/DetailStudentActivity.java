package com.example.aplikasipendaftaransiswabaru;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.aplikasipendaftaransiswabaru.data.DBHelper;
import com.example.aplikasipendaftaransiswabaru.data.Student;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class DetailStudentActivity extends AppCompatActivity {
    private Student student;

    private ImageView imagePreview;
    private TextView name;
    private TextView phone;
    private TextView gender;
    private TextView location;
    private TextView address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_student);

        DBHelper db = new DBHelper(getBaseContext());

        int studentId = getIntent().getIntExtra("student_id", 0);

        student = db.get(studentId);

        imagePreview = findViewById(R.id.image_preview);
        name = findViewById(R.id.name);
        phone = findViewById(R.id.phone);
        gender = findViewById(R.id.gender);
        location = findViewById(R.id.location);
        address = findViewById(R.id.address);

        try {
            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            File f = new File(directory, student.getImage());

            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));

            imagePreview.setImageBitmap(b);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        name.setText(student.getName());
        phone.setText(student.getPhone());
        gender.setText(student.getGender());
        location.setText(student.getLocation());
        address.setText(student.getAddress());
    }
}