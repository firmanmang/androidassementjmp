package com.example.aplikasipendaftaransiswabaru;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.aplikasipendaftaransiswabaru.data.Student;

public class StudentArrayAdapter  extends ArrayAdapter<Student> {
    private final Activity context;
    private final Student[] students;

    public StudentArrayAdapter(@NonNull Activity context,
                            @NonNull Student[] students) {
        super(context, R.layout.list_home_item, students);

        this.context = context;
        this.students = students;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_home_item, null, true);

        TextView titleText = rowView.findViewById(R.id.title);
        TextView subtitleText = rowView.findViewById(R.id.subtitle);
        TextView extrasubtitleText = rowView.findViewById(R.id.extrasubtitle);

        titleText.setText(students[position].getName());
        subtitleText.setText(students[position].getGender());
        extrasubtitleText.setText(students[position].getLocation());

        return rowView;
    }
}
